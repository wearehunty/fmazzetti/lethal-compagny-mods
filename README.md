# A simple bunch of mods to improve the lethal experience.

## List of mods

- **BepInEx** 
> Core Mod, requirered by others to works.  
By BepInEx - https://github.com/BepInEx/BepInEx

- **MoreCompagny**
> A stable lobby player count expansion mod. With cosmetics!  
By notnotnotswipez - https://thunderstore.io/c/lethal-company/p/notnotnotswipez/MoreCompany/

- **ShipLobby**
> Let people join late whenever you're in orbit in between missions.  
By tinyhoot -  https://github.com/tinyhoot/ShipLobby

- **ShipLoot**
> Reliably shows the total value of all scrap in your ship.  
By tinyhoot -  https://github.com/tinyhoot/ShipLoot

- **AdditionalSuits**
> Adds 8 more standard suits to your initial closet at the start of the game!  
By AlexCodesGames - https://github.com/RabidCodeHog/LC-Additional-Suits/

- **HelmetCamera**
> For monitoring first person cameras on player's helmets.  
By RickArg - https://github.com/The0therOne/Helmet_Cameras

- **CustomBoomboxMusicFixed**
> Allows you to use your own music in the boombox.  
By rafl - https://github.com/raflchan/CustomBoomboxMusicFixed

- **ObjectVolumeController**
> Allows adjusting the volume on the media objects.  
By FlipMods - https://thunderstore.io/c/lethal-company/p/FlipMods/ObjectVolumeController/

- **BetterTeleporter**
> Better Teleporters for Lethal Company.  
By SirTyler - https://github.com/SirTyler/BetterTeleporter

- **LBtoKG**
> View your items weight in kilograms.  
By Zduniusz - https://thunderstore.io/c/lethal-company/p/Zduniusz/LBtoKG/  

- **24HourClock**
> Change the 12-hour clock to 24-hour clock.  
By Zduniusz - https://thunderstore.io/c/lethal-company/p/Zduniusz/24HourClock/

- **BoomBoxNoPower**
> Boombox won't use energy anymore!.  
By TV23 - https://www.instagram.com/tear.asunder/

- **NoBackgroundAudio**
> This plugin disables background audio for the game when alt-tabbed(unfocused).  
By MoreLethal - https://fofx.zip/

- **YippeeMod**
> Changes the chitter SFX of the Hoarding Bug to the yippee-tbh sound.  
By sunnobunno - https://thunderstore.io/c/lethal-company/p/sunnobunno/YippeeMod/

- **EladsHUD**
> Replaces the player hud with a different one.  
By EladNLG - https://discord.gg/XeyYqRdRGC  

- **HideChat**
> Fully fade out the text chat when not in use.  
By Monkeytype - https://thunderstore.io/c/lethal-company/p/Monkeytype/HideChat/

- **AlwaysHearActiveWalkies**
> Allows you to hear active walkies even when you're not holding them.  
By Suskitech - https://github.com/n33kos/AlwaysHearActiveWalkie

- **LethalEscape**
> A mod that aims to allow almost every inside creature to escape to the outside so that nowhere is safe!  
By xCeezy - https://thunderstore.io/c/lethal-company/p/xCeezy/LethalEscape/

- **Boombox Sync Fix**
> This mod fixes a base game bug where an already spawned boombox does not play the same song between the client and host.  
By FutureSavior - https://github.com/milleroski/BoomboxSyncFix

- **SuitSaver**
> A plugin for saving your last used suit. (Compatible with v45).  
By Daisuu - https://github.com/Hexnet111/SuitSaver

- **Boombox Autonext**
> Automatically plays the next song in the boombox playlist when the current song ends.  
By Rattenbonkers - https://github.com/RattenBonkers

- **Mimics**
> Adds a dangerous new monster to the game. Can you figure out what's real or will you be devoured?  
By x753 - https://github.com/x753/Lethal-Company-Mimics

- **TooManyEmotes**
> Adds over 250 new emotes! Includes emote wheel, allows Masked Enemies to emote, adds emote music (can be muted), implements a system for purchasing emotes on the store (optional), and much more!  
By FlipMods - https://github.com/cmooref17/Lethal-Company-TooManyEmotes

- **Skinwalkers**
> The monsters have learned how to mimic the voices of your friends.  
By RugbugRedfern -  https://rugbug.net/skinwalkers

## Installation

If you are installing this manually, do the following

1. Download the source code (.zip) from this repository
2. Extract the content of the `lethal-company-mods-main` folder into the game folder (where the game executable is located, by default `C:\Program Files (x86)\Steam\steamapps\common\Lethal Company\`).
3. Run the game. If everything runs correctly, you will enjoy your gaming experince.